import Vue from 'vue'
import App from './App.vue'

new Vue({
  el: '#app',
  render: h => h(App)
})

export class UsersService {
  urlAPI = 'http://localhost:4000/';

  getUsers() {
    const employees = `${this.urlAPI}empleados/`;
    return this.http.get(employees);
  }
  getDepartments() {
    const employees = `${this.urlAPI}departamentos/`;
    return this.http.get(employees);
  }
  postUser() {
    return this.http
      .post(`${this.urlAPI}new-empleado/`, JSON.stringify(user), httpOptions)
      .toPromise()
      .then(res => res.toString());
  }
  
}

